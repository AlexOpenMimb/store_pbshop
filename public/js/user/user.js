const handleState = (id, status) => {
    if (status == 'Activo') {
        status = 'Desactivado'
    }else{
        status = 'Activo'
    }

    Swal.fire({
        title: '¿Cambiar el estado?',
        text: "¡Puedos cambiarlos cuando quieras!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, ¿'+`${status}`+'?'
        }).then((result) => {
        if (result.isConfirmed) {
            window.livewire.emit('handleStatus', id, status)
            Swal.fire(
            'Actualizado!',
            'Ha sido actualizado',
            'success'
            )
        }
    })
}

import {  formatCurrency, getEmailClient, getMessageClient } from "./helper.js";
import { seewtalert } from "./seewtalert.js";

document.addEventListener('DOMContentLoaded',(e)=>{

    getEmailClient();
    seewtalert('.form_category_delete',"Se eliminaran los prodoctos de esta categoría");
    seewtalert('.form_product_delete',"Esta acción no se podra revertir");
    seewtalert('.form_user_delete',"Esta acción no se podra revertir");
    formatCurrency('.cell_price');
    getMessageClient();
   
})
   

// "You won't be able to revert this!"
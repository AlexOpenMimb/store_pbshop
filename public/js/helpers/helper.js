const d = document,
$modalBodyEmail = d.getElementById('idModalBodyClient'),
$modalBodyMessage = d.getElementById('idModalBodyMessage'),
$emailClient =  d.querySelectorAll('.email_client');









export const formatCurrency = (elemet) => {

    const $cell = d.querySelectorAll(elemet);

    $cell.forEach((el)=>{
        let number =  parseInt(el.textContent, 10);
        let format = new Intl.NumberFormat('eng-US', { style: 'currency', currency: 'USD',minimumFractionDigits:0 }).format(number);
        el.textContent = format;  
    })
    
}

//Rendered client´s email into the modal.

export const getEmailClient = () => {

    d.addEventListener('click',(e)=>{

        if(e.target.matches('#idBtnModalClient')){
         
          let textHtml='';
          
          $emailClient.forEach(el=>{
            
            textHtml += ` ${el.innerText},  `;
            
          });
         
          $modalBodyEmail.textContent = textHtml;

        }
    })
}

export const getMessageClient = () => {

  d.addEventListener('click',(e)=>{

    if(e.target.matches('#btn_show_message')){

     let  $message = e.path[3].children[3].children[0].innerText;

     $modalBodyMessage.textContent = $message;
   
    }
})


}



export default function hamburguerMenu() {
    d.addEventListener("click", (e) => {
      if (e.target.matches('#menu-btn *')) {

        e.path[1].firstElementChild.classList.toggle("d-none");
        e.path[1].lastElementChild.classList.toggle("d-none");
        d.querySelector('.menu').classList.toggle("menu--active");
      }
    });
  }

  hamburguerMenu();

  


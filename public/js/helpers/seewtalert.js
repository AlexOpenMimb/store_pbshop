export function seewtalert(form,message){

  $(form).submit(function(e){

    e.preventDefault();

  Swal.fire({
  title: '¿ Estás seguro?',
  text: message,
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, borrar!'
  }).then((result) => {
  if (result.isConfirmed) {

   this.submit();

  }
})

})

}

window


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'photo',
        'slug',
        'description',
        'status',
    ];

    

    const CATEGORY = [
        'HOGAR',
        'COCINA',
        'TECNOLOGÍA',
        'ROPA Y CALZADO',
        'ACCESORIOS DEPORTIVOS',
        'RELOJ Y ACCESORIOS',
        'INFANTIL',
        'SALUD Y BELLEZA',

    ];

     //Relacione

     public function products()
     {
         return $this->hasMany(Product::class);
     }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'products';

    protected $fillable = [
        'reference',
        'name',
        'type',
        'slug',
        'price',
        'photo',
        'category_id',
        'status',
    ];

    const ACTIVE = 'ACTIVO';

    const PRODUCTS = [
        'Jabon',
        'Celular',
        'Cartera',
        'Zapato',
    ];


    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}

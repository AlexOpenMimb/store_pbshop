<?php

namespace App\Http\Livewire;

use App\Models\Role;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public $pagination = 6, $roles;
    public $name, $email, $password, $role_id = 'elegir', $status = 'elegir', $selected_id, $edit = 1, $repeatNewPassword, $newPassword;



    public function mount() 
    {
        $this->roles = Role::all();
      
    }

    public function render()
    {
        $users = User::handleUser();

        return view('livewire.user.component',[

          'users'=> $users->orderBy('id','desc')->paginate($this->pagination)

        ]);
    }



    public function updatingSearch(){
        $this->gotoPage(1);
    }

    public function handelAction()
    {
      $this->Handlereset();
    }

    protected $messages = [
      'name.required'     => 'El campo nombre es requerido.',
      'email.unique'      => 'El email éxite.',
      'email.required'    => 'El campo email debe ser un email.',
      'password.required' => 'El campo contraseña es requerido.',
      'role_id.required'  => 'Debes seleccionar un rol.',
      'status.required'   => 'Debes seleccionar un estado.',
  ];

    public function storeOrUPdate($edit)
    {
     $this->validate([

      'name'     => 'bail|required|min:3|max:20|string',
      'email'    =>  ['bail','required','email','min:4','max:100', Rule::unique('users')->ignore($this->selected_id)->whereNull('deleted_at')],
      'password' => 'bail|required|string',
      'role_id'  => 'bail|required|not_in:elegir',
      'status'   => 'bail|required|not_in:elegir'

     ]);

      $find = [
        'id' => $this->selected_id
      ];
    
      $data = [
        'name'     => $this->name,
        'slug'     => Str::slug($this->name,'-'),
        'email'    => $this->email,
        'role_id'  => $this->role_id,
        'status'   => $this->status,
      ];

     if(!$this->selected_id){
      $data['password'] =  Hash::make($this->password);
     }


     $user = User::updateOrCreate($find,$data);


     $this->emit('modalsClosed');
     $this->Handlereset();
   

    }

    public function edit(User $user,$edit)
    {
      $this->name         = $user->name;
      $this->email        = $user->email;
      $this->password     = $user->password;
      $this->role_id      = $user->role_id;
      $this->edit         = $edit;
      $this->status       = $user->status;
      $this->selected_id  = $user->id;

    }

    // listen for events and execute requested action
    protected $listeners = ['handleStatus'];

   
    public function Handlereset()
    {
      $this->name = '';
      $this->email = '';
      $this->password = '';
      $this->role_id = '';
      $this->status = 'elegir';
      $this->edit = 1;
      $this->selected_id = null;
      $this->newPassword = '';
      $this->repeatNewPassword ='';

    }


    public function handleStatus(User $user, $status)
    {
       
       $user->status = $status;
       $user->save();
    }

    public function handlePassword(User $user){

      dd($user->name);
      if ($this->newPassword) {
          if ($this->newPassword == $this->repeatNewPassword) {

              $user->update([
                  'password' => $this->repeatNewPassword
              ]);


              $this->emit('modalsClosed');
            
          }else{
              // $this->emit('msg-error','Passwords do not match');
          }
      }else{
          // $this->emit('msg-error','Incorrect value, please enter a password');
      }
  }
  
}

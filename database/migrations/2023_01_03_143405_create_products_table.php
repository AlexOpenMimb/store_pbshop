<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment('Nombre de la categoría');
            $table->string('slug');
            $table->string('photo')->nullable();
            $table->text('description')->comment('Descripción de la categoría')->nullable();
            $table->enum('status',['Activo','Desactivado'])->default('Activo');

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->comment('Referencia de producto')->nullable();
            $table->string('name')->comment('Nombre de producto');
            $table->string('slug');
            $table->enum('type',['Nuevo','Stock'])->default('Stock');
            $table->float('price')->comment('precio de producto')->nullable();
            $table->string('photo')->comment('imagen de producto')->nullable();
            
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            
            $table->enum('status',['Activo','Desactivado'])->default('Activo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
        Schema::dropIfExists('products');
    }
}

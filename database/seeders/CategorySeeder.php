<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < count(Category::CATEGORY) ; $i++) { 
            
            Category::create([
                'name' => Category::CATEGORY[$i],
                'slug'        => Str::slug(Category::CATEGORY[$i],'-'),
                'description' => 'Descripción de la categoría',

            ]);
        }
    }
}

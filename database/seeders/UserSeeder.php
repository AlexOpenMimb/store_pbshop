<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'           =>  'Alex Hurtado',
            'slug'           =>  'alex-hurtado',
            'email'          =>  'alex@hot.com',
            'password'       =>   Hash::make('123456789'),
            'role_id'        =>   Role::all()->random()->id
        ]);
        User::create([
            'name'           =>  'Juan Camarto',
            'slug'           =>  'juan-camarto',
            'email'          =>  'juan@hot.com',
            'password'       =>   Hash::make('123456789'),
            'role_id'        =>   Role::all()->random()->id
        ]);
    }
}

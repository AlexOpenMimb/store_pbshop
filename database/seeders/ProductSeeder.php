<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < count(Product::PRODUCTS) ; $i++) { 
            
            Product::create([

                'reference'     => 'PB'.rand(1000, 200000),
                'price'         => rand(1000, 200000),
                'name'          => Product::PRODUCTS[$i],
                'slug'          => Str::slug(Product::PRODUCTS[$i],'-'),
                'category_id'   => Category::all()->random()->id,
            ]);
    }
    }
}

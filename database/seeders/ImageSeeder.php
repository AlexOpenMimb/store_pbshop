<?php

namespace Database\Seeders;

use App\Models\Image;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for ($i=0; $i < count(Image::TYPE); $i++) {
            $name = Image::TYPE[$i];
            Image::create([
                'name'  => $name,
                'slug'  => Str::slug($name,'-'),
            ]);
        }
    }
}

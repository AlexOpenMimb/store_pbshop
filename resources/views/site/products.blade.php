@extends('layouts.template')

@section('content')

<section class="hero-image">
           
    <picture >
            <source srcset="{{asset('assets/img/hero-image-length.webp')}}" media="(min-width:768px)"> 
            <img src="{{asset('assets/img/hero-image-medium.webp')}}" alt="Tecnología y celulares">
        
    </picture>
  
</section>


<section class="container-catalogue container-product">

    <div class="catalogue__title">
        <div>
            <div><p class="catalogue__small">CATALAGO</p></div>
          
                <h2 class="catalogue__text"></h2>              
                <div class="catalogue__line">line</div>
          
        </div>
    </div>
    <div class="d-flex j-c-center">  

       <div class="card-container d-grid ">

    
        <div class="alert alert-danger" role="alert">
           No hay productos  para mostar a los compradores. Agregar productos a la base de datos
        </div>  
    

   
        
            <div class="card-product">
                
                <div><img src="" alt="" loading="lazy"></div>
                <div class="card__name"><span title=""></span></div>
                <div class="card__price"><span>$100.000</span></div>
                <div><a class="c-white-hover card__btn" href="https://api.whatsapp.com/send?phone=573046680596&text=Hola%2C%20me%20interesa%20este%20producto" target="_blank">COMPRAR</a></div>
              
            </div> 
            
            
     
       </div>

    </div>
</section>


@endsection
@extends('layouts.template')

@section('content')

<main>
  @include('layouts.hero')

  <section class="container-catalogue container-product">

    <div class="catalogue__title mt-5">
      <div>
            <h2 class="catalogue__text">Contacto</h2>              
              <div class="catalogue__line">line</div>
        
      </div>
   </div> 
   
   <div class="container mt-5" style="height: 2rem;">
      
    @include('layouts.alert')
   
  </div>
  
    <div class="container d-flex justify-content-center align-items-center mt-5">
  
      <div class="">
            <h3 class="c-gray-3">Registre sus datos en el siguiente formulario</h3>
    
            <form class="mt-4" action="" method="POST">
              @csrf
    
                  <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label c-gray-3">Nombre</label>
                    <input type="text" class="form-control" id="" placeholder="Nombre completo" name="name" required>
                  </div>
                  @error('name')
                  <div id="" class="form-text text-red">* {{$message}}</div>
                 @enderror
    
    
                  <div class="mb-3">
                    <label for="idEmailClient" class="form-label c-gray-3">Email</label>
                       <input type="email" class="form-control" id="idEmailClient" placeholder="name@example.com" name="email" required>
                 </div>
    
                 @error('email')
                 <div id="" class="form-text text-red">* {{$message}}</div>
                @enderror
                 
    
                 
                 <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label c-gray-3">Mensaje</label>
                  <textarea name="message" id="" cols="30" rows="10" class="form-control n-resize" required></textarea>
                </div>
  
                   
                @error('message')
                <div id="" class="form-text text-red">* {{$message}}</div>
                
                @enderror
  
                <div class="d-flex">
                  <input class="mx-2" type="checkbox" name="term" value="" required>
                  <p class="">Acepto y he leído la Autorización para el tratamiento de <a class="text-primary" href="#" download="">datos personales y política de tratamiento de datos.</a> </p>
               
                </div>
            
              
                <button type="submit" class="btn btn-primary">ENVIAR</button>
    
            </form>
            
      </div>
          
    </div>

  </section>



</main>




@endsection
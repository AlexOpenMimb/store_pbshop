@extends('layouts.template')

@section('content')


<main>

    @include('layouts.hero')

    <section class="container-product" >
        <div> 
            <div class="products-new__title-small"><p>NUEVOS</p></div>    
            <div class="products-new__title-big"><p>PRODUCTOS</p></div>  
    
        </div>

        <div class="d-flex j-c-center"> 
                         
                <div class="card-container card-container--home d-grid ">

              
                    <div class="alert alert-danger" role="alert">
                       No hay productos nuevos para mostar a los compradores. Agregar productos a la base de datos
                    </div>  
                  

                    <div class="card_home">          
                        <div><img src="" alt="" loading="lazy"></div>
                        <div class="card__name"><span title=""></span></div>
                        <div class="card__price"><span>$100.000</span></div>
                        <div><a class="card__btn card__btn--home" href="./hogar.html">VER MAS</a></div>
                    
                    </div>

                </div>        
           
        </div>

    
    </section>


    <section class="container-catalogue"> 
    
        <div class="catalogue__title m-top-4 m-bot-4">
            <div>
                <div><p class="catalogue__small">NUESTRO</p></div>
             
                    <h2 class="catalogue__text">CATALAGO</h2>              
                    <div class="catalogue__line">line</div>
               
            </div>
        </div>
        <!-- Card Catalogue -->

        <div class="d-flex j-c-center">
            <article class="d-grid catalogue__grid">

             

                <div class="catalogue__card">
                    <div class="catalogue__card-text"><a href=""></a>
            
                    </div>
                    <div class="catalogue__card-img">
                        <a href="">
                       
                                
                            <img src="" alt="" loading="lazy">
                       
                            <img src="" alt="" loading="lazy">
                                
                       
                        </a>

                    </div>
                </div>
            
               
            </article>

        </div>

    </section> 


</main>

    
@endsection

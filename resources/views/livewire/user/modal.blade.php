
  <!-- Modal -->
  <div wire:ignore.self  class="modal fade" id="createOrUpdateUserId" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">{{$edit === 1 ? 'Crear': 'Actualizar'}} usuario</h5>
          <button type="button" class="btn-close" wire:click="handelAction()" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">

            <form action="">
                <div>
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" wire:model.lazy = "name">
                </div>
            
                <div>
                    <label for="">Email</label>
                    <input type="text" class="form-control" wire:model.lazy = "email">
                </div>
                @if ($edit == 1)
                    
                <div>
                    <label for="">Contraseña</label>
                    <input type="password" class="form-control" wire:model.lazy = "password">
                </div>   
                @endif
          
                <div>
                    <label for="">Rol</label>
                    <select name="" id="" class="form-control" wire:model = "role_id">
                        <option value="elegir">Seleccionar un rol</option>
                        @foreach ($roles as $role)
                           <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach
                    </select>
                   
                </div>
                    
                <div>
                    <label for="">Estado</label>
                    <select name="" class="form-control" wire:model = "status">
                        <option value="elegir">Seleccionar un estado</option>
                        <option value="Activo">Activo</option>
                        <option value="Desactivado">Desactivado</option>
                    </select>
                </div>
             

            </form>
      
            @include('common.message')
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" wire:click="storeOrUPdate(1)">Guardar</button>
            <button type="button" class="btn btn-secondary" wire:click="handelAction()" data-bs-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
  </div>

{{-- Password modal --}}
  <div wire:ignore.self class="modal fade" id="modalChangedPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Cambiar contraseña de usuario</h5>
          <button type="button" class="close"  data-bs-dismiss="modal" aria-label="Close"  wire:click="">
            <span aria-hidden="true" >&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="d-flex flex-column align-items-center justify-content-center">

                <div class="form-group col-lg-8 col-md-8 col-sm-12">
                    <label>Nueva Clave*</label>
                    <input wire:model.lazy="newPassword" type="password" class="form-control"  placeholder="contraseña">
                </div>
                <div class="form-group col-lg-8 col-md-8 col-sm-12">
                    <label>Repetir clave*</label>
                    <input wire:model.lazy="repeatNewPassword" type="password" class="form-control"  placeholder="contraseña">
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-dark" wire:click=""  data-bs-dismiss="modal" aria-label="Close">Cerrar</button>
          <button type="button" class="btn btn-primary" wire:click="handlePassword({{$user->id}})"  >Guardar</button>
        </div>
      </div>
    </div>
</div>

<div>
   
<div class="container">
   <div class="d-flex justify-content-between">
     <h1>Usuario</h1> 
     <div>
      <button type="button" class="btn btn-primary" wire:click="handelAction(1)" data-bs-toggle="modal" data-bs-target="#createOrUpdateUserId">
         CREAR USUARIO
         </button>
     </div>
   </div>
   <table class="table table-striped">
     

      @if (!$users->empty())
         <div>
            <h2>NO hay usuarios registrados</h2>
         </div> 
      @else
      <thead>
         <tr>
           <th scope="col">Item</th>
           <th scope="col">Nombre</th>
           <th scope="col">Correo</th>
           <th scope="col">Rol</th>
           <th scope="col">Esatus</th>
           <th scope="col">Aciones</th>
         </tr>
      </thead>
      <tbody>

         @php($i= 1)
            
         @foreach ($users as $user)
           
         <tr>
               <th scope="row">{{$i}}</th>
               <td>{{$user->name}}</td>
               <td>{{$user->email}}</td>
               <td>{{$user->role->name}}</td>
               <td>{{$user->status}}</td>
               <td>
                  @include('common.action')
                     
               </td>   
         </tr>
         @php($i++)

     @endforeach
          
     @endif

    
          
      </tbody>
   </table>
   <div>

      {{$users->links()}}
   </div>

 </div>

 @include('livewire.user.modal')
 
</div>

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- <title>{{ config('app.name', 'Laravel') }}</title> --}}
    <title>P&B SHOP - Decoración, Hogar, Calzado, Ropa y Tecnología</title>

    <!-- Scripts -->
    {{-- @vite(['resources/js/app.js']) --}}
   <!-- Scripts -->
   <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="icon" href="{{asset('assets/img/logo.jpg')}}">
    <link rel="stylesheet" href="{{ asset('assets/fontello.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}

        <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
 
</head>
<body>

  <header class="container-header" >
    <section class="header">
        <div class="logo">
            <a href="{{route('home')}}">P&B SHOP</a>
        </div>
         <button id="menu-btn" class="menu-btn hamburger-btn c-pointer bck-white">
          <img src="{{asset('assets/img/icon/menu-bars.svg')}}" alt="">
          <img class="d-none" src="{{asset('assets/img/icon/menu-close.svg')}}" alt="">
        </button>
        <div class="header__home">
            <a class="header__home-a d-flex " href="">
               <div>HOME</div>
               <i class="icon-home"></i>
           </a>
        </div>

    </section>
    <nav class="menu">
        <ul class="menu__nav">

    
        
    
          <li class="menu__link">
            <a class="link_a " href=""></a>
          </li> 
    
         


          <li class="menu__link">
            <a class="link_a" href="{{route('contact')}}">Contacto</a>
          </li>

        </ul>
    </nav>
   
</header>


<a class="wpp-btn" href="https://api.whatsapp.com/send?phone=573046680596" target="_blank">
  <i class="icon-whatsapp"></i>
</a>
  @yield('content') 

  <section class="service">
    <div class="service_delivery d-flex">
        
        <div class="service__img">
            <picture>
                
                <source srcset="{{asset('assets/img/mobile_delivery.webp')}}" media="(min-width:1000px)"> 
                <source srcset="{{asset('assets/img/tablet_delivery.webp')}}" media="(min-width:440px)"> 
                
                
                <img src="{{asset('assets/img/tablet_delivery.webp')}}" alt="" loading="lazy">
            </picture>
       </div>
        <div class="service__img">
            <picture>
                <source srcset="{{asset('assets/img/mobile_pay.webp')}}" media="(min-width:1000px)"> 
                <source srcset="{{asset('assets/img/tablet_pay.webp')}}" media="(min-width:440px)"> 
                
                
                <img src="{{asset('assets/img/mobile_pay.webp')}}" alt="" loading="lazy">
            </picture>
       </div>
        
    </div>
</section>

<section class="contact">
    <div class="contact__phone">
        <div class="contact__title"><p>LÍNEA P&B SHOP</p></div>
        <div class="contact__number"><p>+57 304 668 05 96</p></div>
    </div>
    <div class="contact__social">
        <div class="contact_social-title"><p>SÍGUENOS</p></div>
        <div class="contact_social-icon d-flex">
            <a href="https://www.facebook.com/PYBSHOPCOLOMBIA/" target="_blank"> <i class="icon-facebook-squared c-black"></i></a>
            <a href="https://www.instagram.com/pybshop_colombia" target="_blank"> <i class="icon-instagram c-black"></i></i></a>
        
           
            
        </div>
    </div>
</section>


  
  <footer class="footer d-flex">
    <div class="footer__brand"><h4>© P&B SHOP 2002 </h4></div>
    <div class="footer__copy"><h4>TODOS LOS DERECHO RESERVADOS</h4></div>
</footer>
<script type="module" src="{!! asset('js/helpers/helper.js') !!}"></script>
</body>
</html>

<section class="hero-image">
           
    <picture >
   
            <source srcset="{{asset('assets/img/hero-image-length.webp')}}" media="(min-width:768px)"> 
            <img src="{{asset('assets/img/hero-image-medium.webp')}}" alt="Tecnología y celulares">          
   
      
    </picture>
    
</section>
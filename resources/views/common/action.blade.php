<div class="d-flex">
      
    <div class="mx-1">
          <button title="Actualizar" type="button" class="btn btn-primary" id="" data-bs-toggle="modal" data-bs-target="#createOrUpdateUserId" wire:click="edit({{$user->id}},2)"><i class="icon-edit"></i></button>
    </div>
    
    <div class="mx-1">
       <button title="Activar/Desactivar" type="button" class="btn btn-success" id="btnUserActive" onclick="handleState({{$user->id}}, '{{$user->status}}')"><i class="icon-exchange"></i></button>
    </div>

    
    <div class="mx-1">
       <button title="Cambiar Contraseña" type="button" class="btn btn-primary" id="btnUserPassword" onclick="" data-bs-toggle="modal" data-bs-target="#modalChangedPassword" ><i class="icon-key"></i></button>
    </div>
    
 </div>